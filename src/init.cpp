#include <Godot.hpp>
#include <Reference.hpp>
#include <Node.hpp>

using namespace godot;

class SDL_Core : public Reference {
    GODOT_CLASS(SDL_Core, Reference);
public:
    SDL_Core() { }

    /** `_init` must exist as it is called by Godot. */
    void _init() { }

    void test_void_method() {
        Godot::print("This is test");
    }

    Variant method(Variant arg) {
        Variant ret;
        ret = arg;

        return ret;
    }

    static void _register_methods() {
        register_method("method", &SDL_Core::method);
        register_property<SDL_Core, String>("base/name", &SDL_Core::_name, String("SimpleClass"));
    }

    String _name;
    int _value;

    void set_value(int p_value) {
        _value = p_value;
    }

    int get_value() const {
        return _value;
    }
};

class SDL : public Node {
    GODOT_CLASS(SDL, Node);
public:
    SDL() { }
    ~SDL() { }
    void _init() { }
    static void _register_methods() {}
};

/** GDNative Initialize **/
extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

/** GDNative Terminate **/
extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

/** NativeScript Initialize **/
extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);

    godot::register_class<SDL_Core>();
    godot::register_class<SDL>();
}