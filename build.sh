#!/bin/bash

main()
{
    buildOS linux
    #buildOS windows
}

buildOS()
{
    OS=$1
    echo building $OS
    git submodule sync --recursive
    git submodule update --init --recursive

    if [[ ! -f "api.json" ]] #todo add godot to that the builder container (also wget)
    then
        echo "api doesn't exist, so I'm making it"
        godot --gdnative-generate-json-api api.json 
        rm godot-cpp/bin/libgodot-cpp.*.a -f
    fi
    if [[ ! -f "godot-cpp/bin/libgodot-cpp.${OS}.debug.64.a" ]]
    then
        pushd godot-cpp
        scons platform=${OS} generate_bindings=yes -j$(nproc) bits=64 use_custom_api_file=yes custom_api_file=../api.json
        popd
    fi
    
    scons platform=${OS} -j$(nproc)
}


pushd $(dirname $0)
echo 'hit dir name' $(pwd)
main
popd